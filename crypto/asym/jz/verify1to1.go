package jz

import (
	"bytes"
	"context"
	"encoding/hex"
	"fmt"

	jzhash "chainmaker.org/chainmaker/common/v2/crypto/asym/jz/hash"
	v1 "chainmaker.org/shiqinfeng1/vc-server/api/vc/v1"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

func verifyArgsToCA1to1(ctx context.Context, sab, tab, hashSab, hashTab, macSign, hashData, txnID string) (*v1.Verify1To1Res, error) {

	req := &v1.Verify1To1Req{
		TxnID:    txnID,
		HashData: hashData,
		MacSign:  macSign,
		HashSab:  hashSab,
		HashTab:  hashTab,
		Sab:      sab,
		Tab:      tab,
	}

	resp, err := vcClient.Verify1To1(ctx, req)
	if err != nil {
		return nil, err
	}
	if resp == nil {
		return nil, fmt.Errorf("VC验签:无数据返回")
	}

	return resp, nil
}

func verify1to1(ctx context.Context, digest []byte, signed *Signed1to1) (bool, error) {
	// 用t解密MACdata
	macData := signed.PublicData.MacData
	tarti := signed.EncData.Tarti
	if len(macData) != 32 || len(tarti) != 32 {
		fmt.Printf("macData=%v != 32 || tarti=%v != 32\n", len(macData), len(tarti))
		return false, fmt.Errorf("len of macData:%v, tarti:%v not match", macData, tarti)
	}
	tmp, err := jzhash.Xor(macData, tarti)
	if err != nil {
		fmt.Println("check macdata fail")
		return false, fmt.Errorf("Xor(macData:%v, tarti:%v) fail", macData, tarti)
	}
	hashData := tmp[:16]
	parti := tmp[16:]
	// 计算数据的hash
	var data []byte
	pkey := signed.PublicData.PublicKey
	txnID, err := primitive.ObjectIDFromHex(hex.EncodeToString(signed.TxnID[:]))
	if err != nil {
		fmt.Println("txnid is invalid")
		return false, fmt.Errorf("事务id非法")
	}
	data, err = packingArtifacts1to1(ctx, digest, pkey, txnID[:])
	if err != nil {
		fmt.Println("packing success")
		return false, fmt.Errorf("重组签名数据失败")
	}
	sarti := signed.EncData.Sarti
	myHashData := jzhash.GetHash(ctx, parti, sarti, data, uint64(len(data)))

	// 验证hash是否一致
	if !bytes.Equal(hashData, myHashData) {
		fmt.Println("data hash not match")
		return false, fmt.Errorf("签名数据被篡改")
	}
	// 计算sab和tab的hash
	sab := signed.EncData.Sab
	tab := signed.EncData.Tab
	hashSab := jzhash.SHA256(sab)
	hashTab := jzhash.SHA256(tab)
	// 发送验证参数给ca
	VerifyResp, err := verifyArgsToCA1to1(ctx,
		hex.EncodeToString(signed.EncData.Sab),
		hex.EncodeToString(signed.EncData.Tab),
		hashSab, hashTab,
		hex.EncodeToString(signed.PublicData.MacSign),
		hex.EncodeToString(hashData),
		signed.TxnID.Hex())
	if err != nil {
		fmt.Println("verify fail from vc")
		return false, fmt.Errorf("vc验签失败")
	}
	// 验证sha3
	sac, _ := hex.DecodeString(VerifyResp.Sac)
	tac, _ := hex.DecodeString(VerifyResp.Tac)
	if VerifyResp.HashSac != jzhash.SHA256(sac) || VerifyResp.HashTac != jzhash.SHA256(tac) {
		fmt.Println("args hash check fail")
		return false, fmt.Errorf("签名参数被篡改")

	}

	// 用t解密MACdata
	tmp2, _ := jzhash.Xor(tab, tac)
	macSign := signed.PublicData.MacSign
	tmp3, _ := jzhash.Xor(macSign, tmp2)
	hashSign := tmp3[:16]
	Psign := tmp3[16:]

	Ssign, _ := jzhash.Xor(sab, sac)
	otid, _ := hex.DecodeString(VerifyResp.Otid)
	sign := append(hashData, otid...)
	myHashSign := jzhash.GetHash(ctx, Psign, Ssign, sign, uint64(len(sign)))
	if !bytes.Equal(hashSign, myHashSign) {
		fmt.Println("verify sign hash fail")
		return false, nil
	}
	fmt.Println("verify sign hash success")
	return true, nil
}
