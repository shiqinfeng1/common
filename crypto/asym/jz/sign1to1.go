package jz

import (
	"context"
	"encoding/hex"
	"fmt"

	"chainmaker.org/chainmaker/common/v2/crypto/asym/jz/hash"
	v1 "chainmaker.org/shiqinfeng1/vc-server/api/vc/v1"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

type Signed1to1 struct {
	TxnID      primitive.ObjectID   `json:"txn_id"`
	EncData    SignArgsToPeer1to1   `json:"sign_args"`
	PublicData PublicDataToPeer1to1 `json:"public_data"`
}
type SignArgsToPeer1to1 struct {
	Sarti []byte `json:"s_arti" v:"required"`
	Tarti []byte `json:"t_arti" v:"required"`
	Sab   []byte `json:"s_ab" v:"required"`
	Tab   []byte `json:"t_ab" v:"required"`
}
type PublicDataToPeer1to1 struct {
	MacData   []byte `json:"mac_data"`
	MacSign   []byte `json:"mac_sign"`
	PublicKey []byte `json:"publicKey"`
}

var vcClient v1.VcClient

func init() {
	conn, connErr := grpc.Dial("localhost:8182", grpc.WithTransportCredentials(insecure.NewCredentials()))
	if connErr != nil {
		panic(fmt.Errorf("dial vc-server:%v", connErr))
	}
	vcClient = v1.NewVcClient(conn)
}

func packingArtifacts1to1(ctx context.Context, contents []byte, pk, txnID []byte) ([]byte, error) {
	var (
		fileLen int64
		bufLen  int64
		buffer  []byte
	)

	fileLen = int64(len(contents))
	bufLen = fileLen + int64(len(pk)+len(txnID))
	buffer = make([]byte, bufLen)
	copy(buffer[:fileLen], contents)
	copy(buffer[fileLen:], pk)
	copy(buffer[fileLen+int64(len(pk)):], txnID)
	return buffer, nil
}
func archiveSignArgs(ctx context.Context,
	publicKey []byte,
	otid, Sotid, Totid, Sac, Tac, TxnID, OTCA string) error {

	req := &v1.SaveSign1To1ArgsReq{
		TxnID:     TxnID,
		Otca:      OTCA,
		PublicKey: hex.EncodeToString(publicKey),
		Sotid:     Sotid,
		Totid:     Totid,
		Sac:       Sac,
		Tac:       Tac,
		OTID:      otid,
	}
	resp, err := vcClient.SaveSign1To1Args(ctx, req)
	if err != nil {
		return fmt.Errorf("签名失败,请重新签名,错误码:10004:%v", err)
	}
	if resp == nil {
		return fmt.Errorf("签名失败,请重新签名,错误码:10005")
	}
	return nil
}
func sign1To1(ctx context.Context, privateKey, publicKey, digest []byte) (*Signed1to1, error) {
	// 构造加密参数
	qArgs := hash.NewQArgs1to1(ctx)
	if qArgs == nil {
		return nil, fmt.Errorf("签名失败,请重新签名,错误码:10001")
	}
	//打包签名材料
	var (
		data []byte
		err  error
	)
	if data, err = packingArtifacts1to1(ctx, digest, publicKey, qArgs.TxnID[:]); err != nil {
		return nil, fmt.Errorf("签名失败,请重新签名,错误码:10003")
	}

	// 计算隐私身份的hash
	hashPrivID := hash.GetHash(ctx, qArgs.Potid, qArgs.Sotid, privateKey, uint64(len(privateKey)))
	// 计算数据的hash
	hashData := hash.GetHash(ctx, qArgs.Parti, qArgs.Sarti, data, uint64(len(data)))
	// 计算OTCA,OTID
	OTCA, _ := hash.Xor(append(hashPrivID, qArgs.Potid...), qArgs.Totid)
	OTID, _ := hash.Xor(hashPrivID, qArgs.Totid[:16])

	// 计算MAC, sign, hashSign
	macData, _ := hash.Xor(append(hashData, qArgs.Parti...), qArgs.Tarti)

	sign := append(hashData, OTID...)
	// 计算sign的hash
	Ssign, _ := hash.Xor(qArgs.Sab, qArgs.Sac)
	hashSign := hash.GetHash(ctx, qArgs.Psign, Ssign, sign, uint64(len(sign)))
	// 计算Macsign
	Tsign, _ := hash.Xor(qArgs.Tab, qArgs.Tac)
	macSign, _ := hash.Xor(append(hashSign, qArgs.Psign...), Tsign)

	err = archiveSignArgs(ctx, publicKey,
		hex.EncodeToString(OTID), hex.EncodeToString(qArgs.Sotid),
		hex.EncodeToString(qArgs.Totid), hex.EncodeToString(qArgs.Sac),
		hex.EncodeToString(qArgs.Tac),
		qArgs.TxnID.Hex(), hex.EncodeToString(OTCA))
	if err != nil {
		// l.Error().Err(err).Send()
		return nil, err
	}

	return &Signed1to1{
		TxnID: qArgs.TxnID,
		EncData: SignArgsToPeer1to1{
			Sarti: qArgs.Sarti,
			Tarti: qArgs.Tarti,
			Sab:   qArgs.Sab,
			Tab:   qArgs.Tab,
		},
		PublicData: PublicDataToPeer1to1{
			MacData:   macData,
			MacSign:   macSign,
			PublicKey: publicKey,
		},
	}, nil
}
