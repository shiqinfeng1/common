/*
Copyright (C) BABEC. All rights reserved.
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package jz

import (
	"bytes"
	"context"
	crypto2 "crypto"
	"crypto/ecdsa"
	"crypto/x509"
	"encoding/hex"
	"encoding/json"
	"encoding/pem"
	"fmt"

	"chainmaker.org/chainmaker/common/v2/crypto"
	"chainmaker.org/chainmaker/common/v2/crypto/hash"
)

const PKTYPE = "JZ PUBLIC KEY"

type JzPublicKey struct {
	K *ecdsa.PublicKey // todo：暂时使用椭圆曲线的公钥
}

func (pk *JzPublicKey) Bytes() ([]byte, error) {
	if pk.K == nil {
		return nil, fmt.Errorf("public key is nil")
	}
	return x509.MarshalPKIXPublicKey(pk.K)
}

func (pk *JzPublicKey) Verify(digest []byte, sig []byte) (bool, error) {
	var ctx = context.Background()
	if sig == nil {
		return false, fmt.Errorf("nil signature")
	}
	var signed Signed1to1
	if err := json.Unmarshal(sig, &signed); err != nil {
		return false, fmt.Errorf("sign Unmarshal:%v", err)
	}
	fmt.Printf("Verify digest:%v signiture:%v\n", hex.EncodeToString(digest), string(sig))
	return verify1to1(ctx, digest, &signed)
}

func (pk *JzPublicKey) VerifyWithOpts(msg []byte, sig []byte, opts *crypto.SignOpts) (bool, error) {
	if opts == nil {
		return pk.Verify(msg, sig)
	}
	dgst, err := hash.Get(opts.Hash, msg)
	if err != nil {
		return false, err
	}
	return pk.Verify(dgst, sig)
}

func (pk *JzPublicKey) Type() crypto.KeyType {
	return crypto.JZSIGN
}

func (pk *JzPublicKey) String() (string, error) {

	pkDER, err := pk.Bytes()
	if err != nil {
		return "", err
	}

	block := &pem.Block{
		Type:  PKTYPE,
		Bytes: pkDER,
	}

	buf := new(bytes.Buffer)
	if err = pem.Encode(buf, block); err != nil {
		return "", err
	}

	return buf.String(), nil
}

func (pk *JzPublicKey) ToStandardKey() crypto2.PublicKey {
	return pk.K
}
