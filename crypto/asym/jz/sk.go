/*
Copyright (C) BABEC. All rights reserved.
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package jz

import (
	"bytes"
	"context"
	crypto2 "crypto"
	"crypto/ecdsa"
	"crypto/elliptic"
	"crypto/rand"
	"crypto/x509"
	"encoding/hex"
	"encoding/json"
	"encoding/pem"
	"fmt"

	"chainmaker.org/chainmaker/common/v2/crypto"
	"chainmaker.org/chainmaker/common/v2/crypto/hash"
)

const SKTYPE = "JZ PRIVATE KEY"

type JzPrivateKey struct {
	K *ecdsa.PrivateKey // todo：借用椭圆曲线的秘钥对
}

type Sig struct {
	MacData []byte `json:"mac_data"`
	MacSign []byte `json:"mac_sign"`
	Now     string `json:"now"`
	Expire  string `json:"expire"`
	Cert    []byte `json:"cert"`
}

func (sk *JzPrivateKey) Bytes() ([]byte, error) {
	if sk.K == nil {
		return nil, fmt.Errorf("private key is nil")
	}

	return x509.MarshalECPrivateKey(sk.K)
}

func (sk *JzPrivateKey) PublicKey() crypto.PublicKey {
	return &JzPublicKey{K: &sk.K.PublicKey}
}

func (sk *JzPrivateKey) Sign(digest []byte) ([]byte, error) {

	var ctx = context.Background()
	pubKey, err := sk.PublicKey().Bytes()
	if err != nil {
		return nil, fmt.Errorf("get publickey:%v", err)
	}
	signs, err := sign1To1(ctx, sk.K.X.Bytes(), pubKey, digest)
	if err != nil {
		return nil, fmt.Errorf("sign1To1:%v", err)
	}
	ret, err := json.Marshal(signs)
	fmt.Printf("sign digest:%v signiture:%v\n", hex.EncodeToString(digest), string(ret))
	return ret, err
}

func (sk *JzPrivateKey) SignWithOpts(msg []byte, opts *crypto.SignOpts) ([]byte, error) {
	if opts == nil {
		return sk.Sign(msg)
	}
	dgst, err := hash.Get(opts.Hash, msg)
	if err != nil {
		return nil, err
	}
	return sk.Sign(dgst)
}

func (sk *JzPrivateKey) Type() crypto.KeyType {
	return sk.PublicKey().Type()
}

func (sk *JzPrivateKey) String() (string, error) {
	skDER, err := sk.Bytes()
	if err != nil {
		return "", err
	}

	block := &pem.Block{
		Type:  SKTYPE, //  todo：暂时借用椭圆曲线的秘钥对
		Bytes: skDER,
	}

	buf := new(bytes.Buffer)
	if err = pem.Encode(buf, block); err != nil {
		return "", err
	}

	return buf.String(), nil
}

func (sk *JzPrivateKey) ToStandardKey() crypto2.PrivateKey {
	return sk.K
}

func New(keyType crypto.KeyType) (crypto.PrivateKey, error) {
	switch keyType {
	case crypto.JZSIGN:
		pri, err := ecdsa.GenerateKey(elliptic.P256(), rand.Reader) // todo：暂时借用椭圆曲线的秘钥对
		if err != nil {
			return nil, err
		}
		return &JzPrivateKey{K: pri}, nil
	}
	return nil, fmt.Errorf("wrong jz keytype:%v", keyType)
}
