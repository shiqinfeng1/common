package hash

import (
	"context"
	"math/rand"
)

/*
*
  - size 随机码的位数
  - kind 0    // 纯数字
    1    // 小写字母
    2    // 大写字母
    3    // 数字、大小写字母
*/
func randomString(size uint, kind int) []byte {
	ikind := kind
	kinds := [][]int{{10, 48}, {26, 97}, {26, 65}}
	rsbytes := make([]byte, size)

	isAll := kind > 2 || kind < 0

	for i := 0; i < int(size); i++ {
		if isAll { // random ikind
			ikind = rand.Intn(3)
		}
		scope, base := kinds[ikind][0], kinds[ikind][1]
		rsbytes[i] = uint8(base + rand.Intn(scope))
	}
	return rsbytes
}

func GetRngSw(ctx context.Context, rngLlen uint) (outRng []byte, err error) {
	ran := randomString(rngLlen, 3)
	// g.Log().Debugf(ctx, "获取伪随机数=%v", hex.EncodeToString(ran))
	return ran, nil
}

// hash的长度为16字节
func GetRng(ctx context.Context, rngLlen uint) (outRng []byte, err error) {
	return GetRngSw(ctx, rngLlen)
}
