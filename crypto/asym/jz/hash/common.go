package hash

import (
	"context"
	"crypto/sha256"
	"encoding/hex"
	"fmt"
	"math/rand"
	"time"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

func init() {
	rand.Seed(time.Now().UnixNano())
}
func reverse(s []byte) []byte {
	var ss = make([]byte, len(s))
	copy(ss, s)
	for i, j := 0, len(ss)-1; i < j; i, j = i+1, j-1 {
		ss[i], ss[j] = ss[j], ss[i]
	}
	return ss
}

func Xor(a, b []byte) ([]byte, error) {
	if len(a) != len(b) {
		return []byte{}, fmt.Errorf("len not match")
	}
	l := len(a)
	c := make([]byte, l)
	for i := 0; i < l; i++ {
		c[i] = a[i] ^ b[i]
	}
	return c, nil
}

type QArgs1to1 struct {
	Potid []byte
	Sotid []byte
	Totid []byte
	Parti []byte
	Sarti []byte
	Tarti []byte
	Psign []byte
	Sab   []byte
	Sac   []byte
	Tab   []byte
	Tac   []byte
	TxnID primitive.ObjectID
}
type QArgs1toN struct {
	Potca []byte
	Sotca []byte
	Totca []byte
	Parti []byte
	Sarti []byte
	Tarti []byte
	TxnID primitive.ObjectID
}

func SHA256(data []byte) string {
	myhash := sha256.New()
	myhash.Write(data)
	bs := myhash.Sum(nil)
	return hex.EncodeToString(bs)
}
func GetTxnID() primitive.ObjectID {
	return primitive.NewObjectID()
}
func NewQArgs1to1(ctx context.Context) *QArgs1to1 {
	var (
		t   []byte
		t2  []byte
		s   []byte
		s2  []byte
		s3  []byte
		s4  []byte
		t5  []byte
		t6  []byte
		err error
	)
	if t, err = GetRng(ctx, 32); err != nil {
		return nil
	}
	if t2, err = GetRng(ctx, 32); err != nil {
		return nil
	}
	if s, err = GetRng(ctx, 16); err != nil {
		return nil
	}
	if s2, err = GetRng(ctx, 16); err != nil {
		return nil
	}
	if s3, err = GetRng(ctx, 16); err != nil {
		return nil
	}
	if s4, err = GetRng(ctx, 16); err != nil {
		return nil
	}
	if t5, err = GetRng(ctx, 32); err != nil {
		return nil
	}
	if t6, err = GetRng(ctx, 32); err != nil {
		return nil
	}
	return &QArgs1to1{
		Potid: GetP(ctx),
		Sotid: s[:],
		Totid: t,
		Parti: GetP(ctx),
		Sarti: s2[:],
		Tarti: t2,
		Psign: GetP(ctx),
		Sab:   s3[:],
		Sac:   s4[:],
		Tab:   t5[:],
		Tac:   t6[:],
		TxnID: GetTxnID(),
	}
}
