/**
 * @company          MatrixTime[All Right Reserved]
 * @brief            Hash相关运算接口实现
 * @author           gyang
 * @email            gyang@jmail.com
 * @version          1.0.0
 * @date:            2022-01-10 17:44:09
 *
 * Revison History:
 * Author      Date(YYYY-MM-DD)    Version     Description of changes
 * gyang        2022-01-10          1.0.0       创建文件
 * ------  ----------------  -------  ----------------------
 *
 */

#include "QuantHash.h"
#include <cstdio>
#include <string.h>

// 定义一个全局的256 bit的0值mask
__m256i mask256zero;
__m256i mask256one;
__m256i mask256two;
// 定义一个全局的128 bit的0值mask
__m128i mask128zero;
__m128i mask128one;
__m128i mask128two;


#ifndef likely
#define likely(x)   __builtin_expect(!!(x), 1)//表示x的值为真的可能性更大
#endif /* likely */
#ifndef unlikely
#define unlikely(x) __builtin_expect(!!(x), 0) //表示x的值为假的可能性更大
#endif /* unlikely */


/** 
 * @brief           计算128bit中1的个数
 * @param   x       128Bit的数
 *                      
 * @return              
 *      返回包含1个数  
 */
int32_t BitCnt128(xmm_t x)
{
#if 0
    int32_t v = 0;
    // 计算低64bit中1的个数
    v = _mm_popcnt_u64(x.m128i_u64[0]);
    // 计算高64bit中1的个数
    v += _mm_popcnt_u64(x.m128i_u64[1]);

    return v;
#else
    return (_mm_popcnt_u64(x.m128i_u64[0]) + _mm_popcnt_u64(x.m128i_u64[1]));
#endif
}

/**
 * @brief   256Bit数左移成256Bit
 *
 * @param
 *      x   待移位的数
 *      n   左移位数
 *
 * @return
 *      x   移位后的数
 */
inline ymm_t RLeftShift256i(const ymm_t &x, uint32_t n)
{
    ymm_t y;
    y.u256 = _mm256_setzero_si256();
    if (0 == n)
    {
        return x;
    }
    else if (n < 128)
    {
        xmm_t Hi, Low;
        // 
        Hi.m128i = _mm_setzero_si128();
        Low.m128i = _mm_setzero_si128();
        Hi.m128i = _mm_or_si128(SHR128(x.m128i[0].m128i, 128 - n),
                SHL128(x.m128i[1].m128i, n));
        Low.m128i = SHL128(x.m128i[0].m128i, n);
        y.u256 = _mm256_loadu2_m128ic(&Hi.m128i, &Low.m128i);
    }
    else                                            
    {                                               
        // 超过128bit,意味着低128位都是0，高128bit都是低位移入128bit之后，再移位得到的
        xmm_t x1;                                   
        x1.m128i = _mm_setzero_si128();             
        x1.m128i = SHL128(x.m128i[0].m128i, n - 128);
        y.u256 = _mm256_loadu2_m128ic(&x1.m128i, &mask128zero);
    }                                               

    return y;
}

/** 
 * @brief   256Bit数左移n位，n小于128
 *  
 * @param 
 *      x   待移位的数
 *      n   左移位数
 *  
 * @return
 *      x   移位后的数
 */ 
inline ymm_t RLeftShift256iLT128(ymm_t x, uint32_t n)
{
    if (0 == n)
    {
        return x;
    }
    else
    {
        xmm_t Hi, Low;
        //  
        Hi.m128i = _mm_setzero_si128();
        Low.m128i = _mm_setzero_si128();
        Hi.m128i = _mm_or_si128(SHR128(x.m128i[0].m128i, 128 - n),
                SHL128(x.m128i[1].m128i, n));
        Low.m128i = SHL128(x.m128i[0].m128i, n);

        ymm_t y;
        y.u256 = _mm256_setzero_si256();
        y.u256 = _mm256_loadu2_m128ic(&Hi.m128i, &Low.m128i);

        return y;
    }

}

/** 
 * @brief   按bit位混洗数据，最后完成外洗牌
 *          注：这里的输入必须保证已经完成单字节与00相互交错,才能调用
 *  
 * @param 
 *      x   待混洗的数据
 *  
 * @return
 */ 
inline void ShuffleBit256(ymm_t &x)
{
    //到这里了，x的值必须已经完成单字节与0x00交错
    __m256i HalfByte, Cross, HdTl;
    HalfByte = _mm256_setzero_si256();
    Cross = _mm256_setzero_si256();
    HdTl = _mm256_setzero_si256();
    HalfByte = _mm256_set_epi64x(0x0F0F0F0F0F0F0F0Fll, 0x0F0F0F0F0F0F0F0Fll,
            0x0F0F0F0F0F0F0F0Fll, 0x0F0F0F0F0F0F0F0Fll);
    Cross = _mm256_set_epi64x(0x3333333333333333ll, 0x3333333333333333ll,
            0x3333333333333333ll, 0x3333333333333333ll);
    HdTl = _mm256_set_epi64x(0x5555555555555555ll, 0x5555555555555555ll,
            0x5555555555555555ll, 0x5555555555555555ll);

    // 相邻4bit交换
    x.u256 = _mm256_and_si256(_mm256_or_si256(RLeftShift256iLT128(x, 4).u256, x.u256), HalfByte);
    // 相邻2bit交换
    x.u256 = _mm256_and_si256(_mm256_or_si256(RLeftShift256iLT128(x, 2).u256, x.u256), Cross);
    // 相邻1bit交换
    x.u256 = _mm256_and_si256(_mm256_or_si256(RLeftShift256iLT128(x, 1).u256, x.u256), HdTl);
}


#if 0
/** 
 * @brief   实现128bit的值与0x00完成单字节交错，变成256bit
 *          注意：这里需要avx512指令
 *  
 * @param 
 *      x   待混洗的数据
 *  
 * @return
 *      返回混洗后的数据
 */ 
inline __m256i BinSquareOpt1(__m128i x)
{  
    //实现128bit的值与0x00完成单字节交错
    // 128Bit构造高低两个一样的256bit
    __m256i t;
    t = _mm256_set_m128ic(x, x);

    __m256i zero;
    zero = _mm256_setzero_si256();

    uint32_t mask = 0xAAAAAAAA;
    ymm_t v;
    v.u256 = _mm256_setzero_si256();
    v.u256 = _mm256_mask_blend_epi8(mask, t, zero);
    ShuffleBit256(v);

    return v.u256;
}
#endif

/** 
 * @brief   实现128bit的值与0x00完成单字节交错，变成256bit
 *  
 * @param 
 *      x   待混洗的数据
 *  
 * @return
 *      返回混洗后的数据
 */ 
inline __m256i BinSquareOpt(__m128i x)
{
    //实现128bit的值与0x00完成单字节交错
    // 128Bit构造高低两个一样的256bit
    __m256i t;
    t = _mm256_set_m128ic(x, x);

    __m256i zero;
    zero = _mm256_setzero_si256();
    __m128i hi128, lo128;
    // t的高128bit与00单字节交错
    hi128 = _mm256_extracti128_si256(_mm256_unpackhi_epi8(t, zero), 0);
    // t的低128bit与00单字节交错
    lo128 = _mm256_extracti128_si256(_mm256_unpacklo_epi8(t, zero), 0);

    ymm_t v;
    v.u256 = _mm256_setzero_si256();
    // 将高128bit与低128bit混合起来得到256bit
    v.u256 = _mm256_loadu2_m128ic(&hi128, &lo128);
    ShuffleBit256(v);

    return v.u256;
}

/** 
 * 128bit   异或乘
 * @param   x1       输入待相乘的128bit数
 * @param   x2       输入待相乘的128bit数
 *
 * @brief   例如a[m]异或乘b[m]
 *          实际上就是a1&b1 ^ a2&b2 ^ .... ^ a[m-1]&b[m-1]
 * 
 * @return
 *      返回相乘后后的数y，只有一个bit
 */
uint8_t XorMul128(xmm_t *pX1, xmm_t *pX2)
{
    // 构造全1的128bit的数
    //xmm_t mask, x;
    //mask.m128i = _mm_set1_epi64x(MAX_U64);
    xmm_t x;
    // x1&x2，完成a1&b1 ^ a2&b2 ^ .... ^ a[m-1]&b[m-1]
    x.m128i = _mm_and_si128(pX1->m128i, pX2->m128i);
    // 计算128bit的数中，有多少个1
    // 偶数个1,最后异或后为0， 基数个1异或后为1
    return ((BitCnt128(x) & 1)?1:0);
}

/**
 * @brief   256Bit数加2
 *
 * @param
 *      x   被加数
 *
 * @return
 *      x   计算后的数
 */
int32_t AddTwice(ymm_t &x)
{
    //y.u256 = _mm256_setzero_si256();
    if (x.u64[0] < 0xFFFFFFFFFFFFFFFE) 
    {
        // 没有进位，直接自增
        x.u64[0] += 2;
    }
    else if (x.u64[1] < 0XFFFFFFFFFFFFFFFF)
    {
        x.u64[0] += 2;
        // 低位有进位，高位没有进位，直接自增
        x.u64[1]++;
    }
    else if (x.u64[2] < 0XFFFFFFFFFFFFFFFF)
    {
        x.u64[0] += 2;
        x.u64[1]++;
        // 低位有进位，高位没有进位，直接自增
        x.u64[2]++;
    }
    else
    {
        // 最高64位部分的值只会是1，无需考虑进位问题, 由于值不能修改，也就不能自增了
        //x.u64[3]++;
        return -1;
    }

    //y.u256 = _mm256_loadu_si256(&x.u256);

    return 0;
}

/**
 * @brief   128Bit数左移成256Bit
 *
 * @param
 *      x   待移位的数
 *      n   左移位数
 *
 * @return
 *      x   移位后的数
 */
ymm_t RLeftShift128i(const xmm_t &x, uint32_t n)
{
    ymm_t y;
    if (0 == n)
    {
        y.u256 = _mm256_loadu2_m128ic(&x.m128i, &mask128zero);
    }
    else if (n < 128)
    {
        xmm_t Hi, Low;
        // 
        Hi.m128i = _mm_setzero_si128();
        Low.m128i = _mm_setzero_si128();
        Hi.m128i = SHR128(x.m128i, 128 - n);
        Low.m128i = SHL128(x.m128i, n);
        y.u256 = _mm256_loadu2_m128ic(&Hi.m128i, &Low.m128i);
    }
    else                                            
    {                                               
        // 超过128bit,意味着低128位都是0，高128bit都是低位移入128bit之后，再移位得到的
        xmm_t x1;                                   
        x1.m128i = _mm_setzero_si128();             
        x1.m128i = SHL128(x.m128i, n - 128);
        y.u256 = _mm256_loadu2_m128ic(&x1.m128i, &mask128zero);
    }                                               

    return y;
} 

/**
 * @brief   获取128bit最高有效位
 *
 * @param
 *      x   待计算的数
 *
 * @return
 *      x   返回最高有效位位置
 */
uint32_t GetMsb(const xmm_t &x)
{
    if (unlikely(0 == (x.m128i_u64[0] | x.m128i_u64[1])))
    {
        // 说明x等于0
        return 0;
    }
    //return (__builtin_popcountll(x.m128i_u64[1])?(128 - __builtin_clzll(x.m128i_u64[1])):(64 - __builtin_clzll(x.m128i_u64[0])));
    return ((x.m128i_u64[1])?(128 - __builtin_clzll(x.m128i_u64[1])):(64 - __builtin_clzll(x.m128i_u64[0])));
}

/**
 * @brief   获取256bit最高有效位
 *
 * @param
 *      x   待计算的数
 *
 * @return
 *      x   返回最高有效位位置
 */
uint32_t GetMsb(const ymm_t &y)
{
    if (unlikely(0 == (y.u64[0] | y.u64[1] | y.u64[2] | y.u64[3])))
    {
        // 说明x等于0
        return 0;
    }
    // 高位是否为0，不为0，直接求高位后，返回256-高位前面0的个数计数
    int32_t Hi = GetMsb(y.m128i[1]);

    return ((Hi)?(128 + Hi):GetMsb(y.m128i[0]));
}

/**
 * @brief   获取128bit最高有效位
 *
 * @param
 *      x   待计算的数
 *
 * @return
 *      x   返回最高有效位位置
 */
uint32_t GetMsbLow(const xmm_t &x)
{
    uint64_t Hi, Lo;
    Hi = x.m128i_u64[1];
    Lo = x.m128i_u64[0];
    // 为了性能，这里不在判断是否为0，请注意
    // 仅限于在计算256bit的低128bit的值时使用
    // 如果只求128bit的，这里必须放开
    //if (unlikely(0 == (x.m128i_u64[0] | x.m128i_u64[1])))
    if (unlikely(0 == (Lo | Hi)))
    {
        // 说明x等于0
        return 0;
    }
    // 高位是否为0，不为0，直接求高位后，返回128-高位前面0的个数计数
    //int32_t Hi = __builtin_clzll(x.m128i_u64[1]);
    return (__builtin_popcountll(Hi)?(128 - __builtin_clzll(Hi)):(64 - __builtin_clzll(Lo)));
}

#if 0
/** 
 * @brief   实现128bit的值与0x00完成单字节交错，变成256bit
 *          注意：这里需要avx512指令
 *  
 * @param 
 *      x   待混洗的数据
 *  
 * @return
 *      返回混洗后的数据
 */ 
__m256i BinSquareOpt1(__m128i x)
{  
    //实现128bit的值与0x00完成单字节交错
    // 128Bit构造高低两个一样的256bit
    __m256i t;
    t = _mm256_set_m128ic(x, x);

    __m256i zero;
    zero = _mm256_setzero_si256();

    uint32_t mask = 0xAAAAAAAA;
    ymm_t v;
    v.u256 = _mm256_setzero_si256();
    v.u256 = _mm256_mask_blend_epi8(mask, t, zero);
    ShuffleBit256(v);

    return v.u256;
}
#endif

/**
 * @brief   计算g的平方
 *
 * @param
 *      A   被模数
 *
 * @return
 *      返回平方之后的数
 */
inline ymm_t BinSquare(const xmm_t &A)
{
    ymm_t y, y1, y2;
    xmm_t x;
    y2.u256 = _mm256_setzero_si256();
    y1.u256 = _mm256_setzero_si256();
    y1.u8[0] = 1;
    // 初始化为0
    y.u256 = _mm256_setzero_si256();
    //uint32_t uLen = log2_u128(A);
    uint32_t uLen = GetMsb(A);
    uint8_t uBit = 0;
    for (uint32_t i = 0; i < uLen; ++i)
    {
        x.m128i = SHR128(A.m128i, uLen - 1 - i);
        uBit = x.m128i_u8[0] & 1;
        if (uBit)
        {
            y2 = RLeftShift256i(y1, 2 * (uLen - 1 - i));
            y.u256 = _mm256_or_si256(y.u256, y2.u256);
        }
    }

    return y;
}

/**
 * @brief   计算二进制数的平方, 递归法
 *
 * @param
 *      A   被模数
 *
 * @return
 *      返回平方之后的数
 */
inline int32_t BinMod256(ymm_t &A, uint32_t &uBitA, const ymm_t &B, const uint32_t uBitB)
{
    //if (uBitA < uBitB)
    //{
    //    //printf("uBitA < uBitB, error\tuBitA:%u\tuBitB:%u\n", uBitA, uBitB);
    //    //return -1;
    //    // 如果A小于B（位数上），则取模之后仍旧为A，因此无需计算
    //    return 0;
    //}
    ymm_t y;
    y.u256 = _mm256_setzero_si256();
    // 这里主要在递归前判断过位数了，所以A的位数肯定大于等于B
    y = RLeftShift256i(B, (uBitA - uBitB));
    // 计算A' = A xor B'
    A.u256 = _mm256_xor_si256(A.u256, y.u256);

#if 0
    // 判断A是否为0
    //if (0 == (A.u64[0] | A.u64[1] | A.u64[2] | A.u64[3]))
    if (SimdCmpMask256(A.u256, mask256zero))
    {
        // 出现0值，说明本次异或的129bit最终结果为0，这时候需要看位数是否大于等于129bit，
        // 虽然此时整个值可能都为0(取决于后面的低位bit是否全为0)，但就算后面全为0，也需要继续异或下去
        // 如果全部为0了，使用GetMsb，是无法求出全场，因此这里需要特殊处理
        uBitA = 0;
    }
    else
    {
        // 计算A'位数uBitA'， 比较uBitA' > uBitB
        //uBitA = log2_u256(A);
        uBitA = GetMsb(A);
    }
#else
    uBitA = GetMsb(A);
#endif
    //if (uBitA >= uBitB)
    //{
    //    // 如果小于uBitB，直接返回A', 否则递归调用BinMod
    //    return BinMod256(A, uBitA, B, uBitB);
    //}
    //return 0;
    return (uBitA >= uBitB?BinMod256(A, uBitA, B, uBitB):0);

}

/**
 * @brief   计算二进制数的平方, 递归法
 *
 * @param
 *      A   被模数
 *
 * @return
 *      返回平方之后的数
 */
inline int32_t BinMod256d(ymm_t &A, uint32_t &uBitA, const ymm_t &B, const uint32_t uBitB)
{
    ymm_t y;

    do
    {
        y.u256 = _mm256_setzero_si256();
        y = RLeftShift256i(B, (uBitA - uBitB));
        // 计算A' = A xor B'
        A.u256 = _mm256_xor_si256(A.u256, y.u256);
        //更新uBitA
        uBitA = GetMsb(A);
    } while (uBitA >= uBitB);
    return 0;
}

/**
 * @brief   计算256位的gcd
 *
 * @param
 *      A   被模数
 *      B   模数
 *      C   模后结果
 *
 * @return
 *      0   计算成功
 *      1   计算失败
 */
inline int32_t Gcd256(ymm_t &A, uint32_t &uBitA, ymm_t &B, uint32_t &uBitB)
{
    // 理论上来说uBitA 肯定大于uBitB
    // 因为递归前已经做了判断，A的长度必然大于等于B的长度
    ymm_t y;
    y = RLeftShift256i(B, (uBitA - uBitB));
    A.u256 = _mm256_xor_si256(A.u256, y.u256);

    //uint64_t uValue = A.u64[3] | A.u64[2] | A.u64[1] | A.u64[0];
    //// 比较结果是否为1或者0，决定是否需要继续递归调用
    //if (0 == uValue)
    //if (SimdCmpMask256(A.u256, mask256zero))
    if (0xFFFFFFFF == (uint32_t)_mm256_movemask_epi8(_mm256_cmpeq_epi8(A.u256, mask256zero)))
    {
        // 此时由于递归无序，反正A和B都为相同的值，异或之后为0
        // 这里需要将B的值设置为0，供外面使用
        B.u256 = _mm256_setzero_si256();

        // 该多项式不可用
        return -1;
    }
    // 判断高192位是否为0, 且低64bit为1
    //if (0 == (A.u64[3] | A.u64[2] | A.u64[1]) && 1 == A.u64[0])
    //if (SimdCmpMask256(A.u256, mask256one))
    else if (0xFFFFFFFF == (uint32_t)_mm256_movemask_epi8(_mm256_cmpeq_epi8(A.u256, mask256one)))
    {
        // 该多项式可用
        return 0;
    }
    else
    {
        // 继续递归调用直到最后
        // 计算A'位数uBitA'， 比较uBitA' > uBitB
        //uBitA = log2_u256(A);
        //uBitB = log2_u256(B);
        uBitA = GetMsb(A);
        uBitB = GetMsb(B);
        return (uBitA >= uBitB?Gcd256(A, uBitA, B, uBitB):Gcd256(B, uBitB, A, uBitA));
    }
}

/**
 * @brief   计算256位的gcd, 递归版本
 *
 * @param
 *      A   被模数
 *      B   模数
 *      C   模后结果
 *
 * @return
 *      0   计算成功
 *      1   计算失败
 */
inline int32_t Gcd256Long(ymm_t &A, uint32_t &uBitA, ymm_t &B, uint32_t &uBitB, ymm_t &Res)
{
    // 理论上来说uBitA 肯定大于uBitB
    // 因为递归前已经做了判断，A的长度必然大于等于B的长度
    ymm_t y;
    y = RLeftShift256i(B, (uBitA - uBitB));
    A.u256 = _mm256_xor_si256(A.u256, y.u256);

    // 保存Res
    Res.u256 = _mm256_loadu_si256(&A.u256);

    //uint64_t uValue = A.u64[3] | A.u64[2] | A.u64[1] | A.u64[0];
    // 比较结果是否为1或者0，决定是否需要继续递归调用
    if (0 == (A.u64[3] | A.u64[2] | A.u64[1] | A.u64[0]))
    //if (0xFFFFFFFF == (uint32_t)_mm256_movemask_epi8(_mm256_cmpeq_epi8(A.u256, mask256zero)))
    {
        // 此时由于递归无序，反正A和B都为相同的值，异或之后为0
        // 这里需要将B的值设置为0，供外面使用
        B.u256 = _mm256_setzero_si256();

        // 该多项式不可用
        return -1;
    }
    // 判断高192位是否为0, 且低64bit为1
    //if (0 == (A.u64[3] | A.u64[2] | A.u64[1]) && 1 == A.u64[0])
    else if (0xFFFFFFFFl == _mm256_movemask_epi8(_mm256_cmpeq_epi8(A.u256, mask256one)))
    {
        // 该多项式可用
        return 0;
    }
    else
    {
        // 继续递归调用直到最后
        // 计算A'位数uBitA'， 比较uBitA' > uBitB
        //uBitA = log2_u256(A);
        //uBitB = log2_u256(B);
        uBitA = GetMsb(A);
        uBitB = GetMsb(B);
        //return (uBitA >= uBitB?Gcd256(A, uBitA, B, uBitB):Gcd256(B, uBitB, A, uBitA));
        return (uBitA >= uBitB?Gcd256Long(A, uBitA, B, uBitB, Res):Gcd256Long(B, uBitB, A, uBitA, Res));
    }
}

/**
 * @brief   计算256位的gcd, 迭代版本
 *
 * @param
 *      A   被模数
 *      B   模数
 *      C   模后结果
 *
 * @return
 *      0   计算成功
 *      1   计算失败
 */
inline int32_t Gcd256d(ymm_t &A, uint32_t &uBitA, ymm_t &B, uint32_t &uBitB)
{
    // 理论上来说uBitA 肯定大于uBitB
    // 因为递归前已经做了判断，A的长度必然大于等于B的长度
    ymm_t y, v;
    v.u256 = _mm256_setzero_si256();

    uint32_t uBit = uBitA - uBitB;
    do
    {
        if (uBit > 0)
        {
            y = RLeftShift256i(B, (uBitA - uBitB));
            A.u256 = _mm256_xor_si256(A.u256, y.u256);
            uBitA = GetMsb(A);
            v.u256 = _mm256_load_si256(&A.u256);
        }
        else
        {
            y = RLeftShift256i(A, (uBitB - uBitA));
            B.u256 = _mm256_xor_si256(B.u256, y.u256);
            uBitB = GetMsb(B);
            v.u256 = _mm256_load_si256(&B.u256);
        }
        //uint64_t uValue = A.u64[3] | A.u64[2] | A.u64[1] | A.u64[0];
        //// 比较结果是否为1或者0，决定是否需要继续递归调用
        //if (0 == uValue)
        //if (SimdCmpMask256(A.u256, mask256zero))
        if (unlikely(0xFFFFFFFF == (uint32_t)_mm256_movemask_epi8(_mm256_cmpeq_epi8(v.u256, mask256zero))))
        {
            // 此时由于递归无序，反正A和B都为相同的值，异或之后为0
            // 这里需要将B的值设置为0，供外面使用
            B.u256 = _mm256_setzero_si256();

            // 该多项式不可用
            return -1;
        }
        // 判断高192位是否为0, 且低64bit为1
        //if (0 == (A.u64[3] | A.u64[2] | A.u64[1]) && 1 == A.u64[0])
        //if (SimdCmpMask256(A.u256, mask256one))
        else if (0xFFFFFFFF == (uint32_t)_mm256_movemask_epi8(_mm256_cmpeq_epi8(v.u256, mask256one)))
        {
            // 该多项式可用
            return 0;
        }
        else
        {
            uBit = uBitA - uBitB;
        }

    } while (uBit > 1);

    return 0;
}

/**
 * @brief   计算不可约多项式
 *
 * @param
 *      p   输入的随机数, 输出不可约多项式
 *      Gf  计算得到的不可约多项式
 *
 * @return
 *      返回 状态
 */
//int32_t GetGF(ymm_t &P, ymm_t &Gf)
int32_t GetGF(char *pR256, char *Gf)
{
    ymm_t G;
    G.u256 = _mm256_setzero_si256();

    //xmm_t g;
    // 构造g为65bit有效位，保证g^2之后位数大于128
    //g.m128i = _mm_setzero_si128();
    //g.m128i_u64[1] = 1;

    ymm_t g1;
    g1.u256 = _mm256_setzero_si256();
    g1.u64[2] = 1;

    __m128i lo128;
    lo128 = _mm_setzero_si128();

    ymm_t P;
    P.u256 = _mm256_setzero_si256();
    P.u256 = _mm256_loadu_si256((__m256i *)pR256);

    uint32_t uBitP = GetMsb(P);

    int32_t i = 1;

    // 首次让G直接为[00...1 00..00 00..00]，共129bit
    G.u256 = _mm256_loadu_si256(&g1.u256);
    uint32_t uBitG = 129;


    uint64_t ucnt = 0;

    for (;;)
    {
        // 计算取模
        //BinMod256(G, uBitG, P, uBitP);
        BinMod256d(G, uBitG, P, uBitP);

        // 保存低128位值，节省后面间接寻址
        lo128 = _mm_load_si128(&G.m128i[0].m128i);

        // 如果结果为[0 0 0 ... 0 1]，且P并未发生改变，则后面继续取模没有任何意义，应该直接作废，重新计算
        //if (1 == G.u64[0] && (0 == (G.u64[1] | G.u64[2] | G.u64[3])))
        //if (SimdCmpMask256(G.u256, mask256one))
        if (0xFFFFFFFF == (uint32_t)_mm256_movemask_epi8(_mm256_cmpeq_epi8(G.u256, mask256one)))
        {
            // 走到这里说明计算失败，重新计算
            i = 1;
            AddTwice(P);
            //G = BinSquare(g);
            //G.u256 = BinSquareOpt(g.m128i);
            G.u256 = _mm256_load_si256(&g1.u256);
            continue;
        }

        // 第58次结果需要进行运算后保存
        if (58 == i)
        {
            // 保存第58次结果，异或[0 0 0 ... 0 1]，如果等于0，不符合要求
            //if (SimdCmpMask128(G.m128i[0].m128i, mask128one))
            //if (0xFFFF == _mm_movemask_epi8(_mm_cmpeq_epi8(G.m128i[0].m128i, mask128one)))
            if (0xFFFF == _mm_movemask_epi8(_mm_cmpeq_epi8(lo128, mask128one)))
            {
                // 保存多项式到出参
                //_mm256_store_si256(pGf, P.u256);
                memcpy(Gf, &P.u256, 32);
                break;
            }
            //if (SimdCmpMask128(G.m128i[0].m128i, mask128two))
            //if (0xFFFF == _mm_movemask_epi8(_mm_cmpeq_epi8(G.m128i[0].m128i, mask128two)))
            if (0xFFFF == _mm_movemask_epi8(_mm_cmpeq_epi8(lo128, mask128two)))
            {
                // 第58次结果，等于[0 0 0 0 ... 1 0]，则重新计算
                // 走到这里说明计算失败，重新计算
                i = 1;
                AddTwice(P);
                //G = BinSquare(g);
                //G.u256 = BinSquareOpt(g.m128i);
                G.u256 = _mm256_load_si256(&g1.u256);
                continue;
            }
        }
        if (122 == i)
        {
            ucnt += 122;
            // 迭代122次之后，判断最后结果是否为[0 0 0 0 ... 1 0]
            //if ((0 == G.m128i[0].m128i_u64[1] && 2 == G.m128i[0].m128i_u64[0]))
            //if (SimdCmpMask128(G.m128i[0].m128i, mask128two))
            //if (0xFFFF == _mm_movemask_epi8(_mm_cmpeq_epi8(G.m128i[0].m128i, mask128two)))
            if (0xFFFF == _mm_movemask_epi8(_mm_cmpeq_epi8(lo128, mask128two)))
            {
                // 临时保存多项式
                ymm_t yTmp;
                yTmp.u256 = _mm256_setzero_si256();
                yTmp.u256 = _mm256_loadu_si256(&P.u256);
                // 计算gcd
                if (0 == Gcd256d(P, uBitP, G, uBitG))
                    //if (0 == Gcd256(P, uBitP, G, uBitG))
                {
                    //p此时的值对应的多项式可用
                    //printf("No.122 is ok, P is available\n");

                    // 保存多项式到出参
                    //_mm256_store_si256(pGf, yTmp.u256);
                    memcpy(Gf, &yTmp.u256, 32);
                    break;
                }
            }
            // 不为[0 0 0 0 ... 1 0]，则重新计算
            // 走到这里说明计算失败，重新计算
            i = 1;
            AddTwice(P);
            //G = BinSquare(g);
            G.u256 = _mm256_load_si256(&g1.u256);
            continue;
        }
        i++;
        //G = BinSquare(G.m128i[0]);
        //G.u256 = BinSquareOpt(G.m128i[0].m128i);
        G.u256 = BinSquareOpt(lo128);
        //G.u256 = BinSquareOpt1(lo128);
    }

    _mm256_zeroupper();

    return 0;
}

/** 
 * @brief                 根据多项式和随机数，计算，得到LFSR的一行/列
 * @param   Lfsr          随机数, 也作为出参，进行LFSR运算后的结果
 * @param   p             不可约多项式(p在调用前，需要把最低为置为1)
 * @param   pMatrix       128Bit的，结果
 *
 * @return              
 */
static inline void GetLFSR(xmm_t &Lfsr, xmm_t p)
{
    Bit8 *pBit8 = NULL;

    xmm_t x;
    x.m128i = _mm_and_si128(Lfsr.m128i, p.m128i);
    // 与多项式与，得到有效位
    uint8_t uBit = BitCnt128(x);
    // 128bit 整体右移1位
    Lfsr.m128i = SHR128LT64(_mm_load_si128(&Lfsr.m128i), 1);
    pBit8 = (Bit8 *)&Lfsr;
    (pBit8 + 15)->b7 = uBit;
}

/**
 * @brief 单bit的Hash函数
 *
 * @param pLfsr     LFSR移位矩阵产生的128x1的矩阵
 * @param pHash     Hash结果, 由外部分配内存，否则无法Hash
 *
 * @return 
 */
inline void xor128_1(xmm_t &Lfsr, xmm_t &Hash)
{
    __m128i x1;
    x1 = _mm_load_si128((__m128i*)&Hash);

    // 两个128Bit 的大数异或
    x1 ^= Lfsr.m128i;
    _mm_store_si128((__m128i *)&Hash.m128i, x1);
}

// 按照4bit为单位,计算Hash
inline void SetHash(uint8_t uiMsg, xmm_t &pLfsr, xmm_t p, xmm_t &pHash)
{
    // printf("uiMsg=%x pHash.m128i_u64[0]=%lx %lx\n",uiMsg,pHash.m128i_u64[0],pHash.m128i_u64[1]);
    switch (uiMsg)
    {
        case 0:
            {
                // bit: 0000
                // 全0，跳过，pHash中的Hash值维持不变
                GetLFSR(pLfsr, p);
                GetLFSR(pLfsr, p);
                GetLFSR(pLfsr, p);
                GetLFSR(pLfsr, p);
            }
            break;
        case 1:
            {
                // bit: 0001
                // 更新LFSR矩阵
                GetLFSR(pLfsr, p);
                xor128_1(pLfsr, pHash);
                GetLFSR(pLfsr, p);
                GetLFSR(pLfsr, p);
                GetLFSR(pLfsr, p);
            }
            break;
        case 2:
            {
                // bit: 0010
                // 更新2次LFSR矩阵
                GetLFSR(pLfsr, p);
                GetLFSR(pLfsr, p);
                xor128_1(pLfsr, pHash);
                GetLFSR(pLfsr, p);
                GetLFSR(pLfsr, p);
            }
            break;
        case 3:
            {
                // bit: 0011
                // 更新LFSR矩阵
                GetLFSR(pLfsr, p);
                xor128_1(pLfsr, pHash);
                GetLFSR(pLfsr, p);
                xor128_1(pLfsr, pHash);
                GetLFSR(pLfsr, p);
                GetLFSR(pLfsr, p);
            }
            break;
        case 4:
            {
                // bit: 0100
                // 更新3次LFSR矩阵
                GetLFSR(pLfsr, p);
                GetLFSR(pLfsr, p);
                GetLFSR(pLfsr, p);
                xor128_1(pLfsr, pHash);
                GetLFSR(pLfsr, p);

            }
            break;
        case 5:
            {
                // bit: 0101
                // 更新LFSR矩阵
                GetLFSR(pLfsr, p);
                xor128_1(pLfsr, pHash);
                GetLFSR(pLfsr, p);
                GetLFSR(pLfsr, p);
                xor128_1(pLfsr, pHash);
                GetLFSR(pLfsr, p);
            }
            break;
        case 6:
            {
                // bit: 0110
                // 更新3次LFSR矩阵
                GetLFSR(pLfsr, p);
                GetLFSR(pLfsr, p);
                xor128_1(pLfsr, pHash);
                GetLFSR(pLfsr, p);
                xor128_1(pLfsr, pHash);
                GetLFSR(pLfsr, p);
            }
            break;
        case 7:
            {
                // bit: 0111
                // 更新LFSR矩阵
                GetLFSR(pLfsr, p);
                xor128_1(pLfsr, pHash);
                GetLFSR(pLfsr, p);
                xor128_1(pLfsr, pHash);
                GetLFSR(pLfsr, p);
                xor128_1(pLfsr, pHash);
                GetLFSR(pLfsr, p);
            }
            break;
        case 8:
            {
                // bit: 1000
                // 更新4次LFSR矩阵
                GetLFSR(pLfsr, p);
                GetLFSR(pLfsr, p);
                GetLFSR(pLfsr, p);
                GetLFSR(pLfsr, p);
                xor128_1(pLfsr, pHash);
            }
            break;
        case 9:
            {
                // bit: 1001
                // 更新LFSR矩阵
                GetLFSR(pLfsr, p);
                xor128_1(pLfsr, pHash);
                GetLFSR(pLfsr, p);
                GetLFSR(pLfsr, p);
                GetLFSR(pLfsr, p);
                xor128_1(pLfsr, pHash);
            }
            break;
        case 0xA:
            {
                // bit: 1010
                // 更新LFSR矩阵
                GetLFSR(pLfsr, p);
                GetLFSR(pLfsr, p);
                xor128_1(pLfsr, pHash);
                GetLFSR(pLfsr, p);
                GetLFSR(pLfsr, p);
                xor128_1(pLfsr, pHash);
            }
            break;
        case 0xB:
            {
                // bit: 1011
                // 更新LFSR矩阵
                GetLFSR(pLfsr, p);
                xor128_1(pLfsr, pHash);
                GetLFSR(pLfsr, p);
                xor128_1(pLfsr, pHash);
                GetLFSR(pLfsr, p);
                GetLFSR(pLfsr, p);
                xor128_1(pLfsr, pHash);
            }
            break;
        case 0xC:
            {
                // bit: 1100
                // 更新LFSR矩阵
                GetLFSR(pLfsr, p);
                GetLFSR(pLfsr, p);
                GetLFSR(pLfsr, p);
                xor128_1(pLfsr, pHash);
                GetLFSR(pLfsr, p);
                xor128_1(pLfsr, pHash);
            }
            break;
        case 0xD:
            {
                // bit: 1101
                // 更新LFSR矩阵
                GetLFSR(pLfsr, p);
                xor128_1(pLfsr, pHash);
                GetLFSR(pLfsr, p);
                GetLFSR(pLfsr, p);
                xor128_1(pLfsr, pHash);
                GetLFSR(pLfsr, p);
                xor128_1(pLfsr, pHash);
            }
            break;
        case 0xE:
            {
                // bit: 1110
                // 更新LFSR矩阵
                GetLFSR(pLfsr, p);
                GetLFSR(pLfsr, p);
                xor128_1(pLfsr, pHash);
                GetLFSR(pLfsr, p);
                xor128_1(pLfsr, pHash);
                GetLFSR(pLfsr, p);
                xor128_1(pLfsr, pHash);
            }
            break;
        case 0xF:
            {
                // bit: 1110
                // 更新LFSR矩阵
                GetLFSR(pLfsr, p);
                xor128_1(pLfsr, pHash);
                GetLFSR(pLfsr, p);
                xor128_1(pLfsr, pHash);
                GetLFSR(pLfsr, p);
                xor128_1(pLfsr, pHash);
                GetLFSR(pLfsr, p);
                xor128_1(pLfsr, pHash);
            }
            break;
        default:
            //printf("Invalid path\n");
            break;
    }
}

///**
// * @brief 量子Hash函数
// *
// * @param pMsg      待Hash的明文
// * @param uiLen     待Hash的明文长度
// * @param pRandom   量子随机数，16字节
// * @param p         不可约多项式
// * @param pHash     Hash结果, 由外部分配内存，否则无法Hash
// *
// * @return 
// */
int32_t QuantumHash4C(char *pMsg, uint64_t uiLen, xmm_t &pRandom, xmm_t p, xmm_t &pHash)
{
    // 清0Hash结果内存
    uint8_t uiValue = 0;
    // 循环取明文
    for (uint32_t i = 0; i < uiLen; ++i)
    {
        // 根据各Bit的值来做运算
        // 取低4bit来计算
        uiValue = pMsg[i] & 0xf;
        SetHash(uiValue, pRandom, p, pHash);

        // 取高4bit来计算
        uiValue = (pMsg[i] & 0xf0) >> 4;
        SetHash(uiValue, pRandom, p, pHash);
    }

    return 0;
}

/**
 * @brief 量子Hash函数
 *
 * @param pMsg      待Hash的明文
 * @param uMsgLen   待Hash的明文长度
 * @param pRandom   量子随机数，16字节
 * @param uRLen     不可约多项式长度
 * @param p         不可约多项式
 * @param uPLen     不可约多项式长度
 * @param pHash     Hash结果, 由外部分配内存，否则无法Hash
 * @param uHashLen  Hash结果，长度，这里固定为16字节即128bit
 *
 * @return 
 */
int32_t QuantumHashCommon(char *pMsg, uint64_t uMsgLen, char *pRandom,
        char *p, char *pHash)
{
#if 1
    if (NULL == pMsg || NULL == pRandom || NULL == p || NULL == pHash)
    {
        return -1;
    }
#endif
    // 清0Hash结果内存
    //SetZero(pHash);
    //xmm_t xRandom, xP, xHash; 
    xmm_t xRandom, xP; 
    xRandom.m128i = _mm_setzero_si128();
    xRandom.m128i = _mm_load_si128((__m128i const*)pRandom);
    xP.m128i = _mm_setzero_si128();
    xP.m128i = _mm_load_si128((__m128i const*)p);

    xmm_t *xHash = (xmm_t *)pHash;

    uint8_t uiValue = 0;
    // 循环取明文
    for (uint32_t i = 0; i < uMsgLen; ++i)
    {
        // 根据各Bit的值来做运算
        // 取低4bit来计算
        uiValue = pMsg[i] & 0xf;
        SetHash(uiValue, xRandom, xP, *xHash);

        // 取高4bit来计算
        uiValue = (pMsg[i] & 0xf0) >> 4;
        SetHash(uiValue, xRandom, xP, *xHash);
    }

    _mm256_zeroupper();

    return 0;
}

/* @brief   初始化
 *          使用前务必调用初始化
 */
void Init()
{
    // 初始化全局变量掩码                                                                                                                                                  │▶ TestGetMac/                 
    mask128zero = _mm_setzero_si128();
    mask128one = _mm_set_epi64x(0x0ll, 0x1ll);
    mask128two = _mm_set_epi64x(0x0ll, 0x2ll);
    mask256zero = _mm256_setzero_si256();
    mask256one = _mm256_set_epi64x(0x0ll, 0x0ll, 0x0ll, 0x1ll);
    mask256two = _mm256_set_epi64x(0x0ll, 0x0ll, 0x0ll, 0x2ll);
}



